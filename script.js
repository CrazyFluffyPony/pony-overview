"use strict";


setInterval(()=>{
	const yPosCenter = window.scrollY + $(window).height()/2

	let focusedHeadingId = undefined

	let previousElementEnd = 0

	$('#menu a').each((i, elem) => {
		if(!focusedHeadingId){
			let headingId = $(elem).attr('href')
			if(headingId && headingId.startsWith('#') && $(headingId).length > 0){
				const elementEnd = $(headingId).offset().top + $(headingId).outerHeight()
				if(yPosCenter <= elementEnd){
					focusedHeadingId = headingId
				}
				previousElementEnd = elementEnd
			}
		}
	})


	$('#menu a.selected').removeClass('selected')

	if(focusedHeadingId){
		$('#menu a[href="' + focusedHeadingId + '"]').addClass('selected')
	}

}, 100)